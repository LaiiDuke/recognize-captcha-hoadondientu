import numpy as np
import tensorflow as tf
from flask import Flask, request, jsonify
from tensorflow import keras
from keras import layers

app = Flask(__name__)

myAPIKey = 'thisismyapikey'
reconstructed_model = keras.models.load_model("static/my_model")

repredict_model = keras.models.Model(
    reconstructed_model.get_layer(name="image").input, reconstructed_model.get_layer(name="dense2").output
)
repredict_model.summary()
characters = ['2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p',
              'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z']
char_to_num = layers.StringLookup(
    vocabulary=list(characters), mask_token=None
)
num_to_char = layers.StringLookup(
    vocabulary=char_to_num.get_vocabulary(), mask_token=None, invert=True
)


def encode_single_sample(img_path, label):
    # 1. Read image
    img = tf.io.read_file(img_path)
    # 2. Decode and convert to grayscale
    img = tf.io.decode_png(img, channels=1)
    # 3. Convert to float32 in [0, 1] range
    img = tf.image.convert_image_dtype(img, tf.float32)
    # 4. Resize to the desired size
    img = tf.image.resize(img, [40, 200])
    # 5. Transpose the image because we want the time
    # dimension to correspond to the width of the image.
    img = tf.transpose(img, perm=[1, 0, 2])
    # 6. Map the characters in label to numbers
    label = char_to_num(tf.strings.unicode_split(label, input_encoding="UTF-8"))
    # 7. Return a dict as our model is expecting two inputs
    return {"image": img, "label": label}


def decode_batch_predictions(pred):
    input_len = np.ones(pred.shape[0]) * pred.shape[1]
    # Use greedy search. For complex tasks, you can use beam search
    results = keras.backend.ctc_decode(pred, input_length=input_len, greedy=True)[0][0][
              :, :6
              ]
    # Iterate over the results and get back the text
    output_text = []
    for res in results:
        res = tf.strings.reduce_join(num_to_char(res)).numpy().decode("utf-8")
        output_text.append(res)
    return output_text


@app.route('/')
def hello_world():  # put application's code here
    return 'Hello World!'


@app.route("/recognize", methods=["POST"])
def process_image():
    try:
        apiKey = request.headers['x-api-key']
        if apiKey != myAPIKey:
            return jsonify({'msg': 'error'})
        pred_texts = ''
        file = request.files['image']
        # Read the image via file.stream
        file.save(f"static/image/{file.filename}")

        validation_dataset = tf.data.Dataset.from_tensor_slices(
            ([f"static/image/{file.filename}"], [f"{file.filename}"]))
        validation_dataset = (
            validation_dataset.map(
                encode_single_sample, num_parallel_calls=tf.data.AUTOTUNE
            )
            .batch(1)
            .prefetch(buffer_size=tf.data.AUTOTUNE)
        )
        for batch in validation_dataset.take(1):
            batch_images = batch["image"]
            preds = repredict_model.predict(batch_images)
            pred_texts = decode_batch_predictions(preds)

        return jsonify({'msg': 'success', 'result': pred_texts[0]})
    except:
        return jsonify({'msg': 'error'})


if __name__ == '__main__':
    app.run(host="0.0.0.0")
