FROM python:3.8
WORKDIR app/captcha
COPY . .
COPY static/my_model/* static/my_model/
RUN pip install -r requirements.txt
RUN ls
RUN cd static/my_model
RUN ls
RUN cd ../../
RUN mkdir static/image
EXPOSE 5000
CMD python app.py
